function testString(str) {
    let chars = str.split(''),
        stack = [],
        open = ['(', '['], close = [')', ']'];
    let openBr, closeBr;
    
    for (var i = 0, len = chars.length; i < len; i++) {
       openBr = open.indexOf(chars[i]);
       if (openBr !== -1) {
           stack.push(openBr);
           continue;
       }

       closeBr = close.indexOf(chars[i]);
       if (closeBr !== -1) {
           openBr = stack.pop();
           if (closeBr !== openBr) { return false; }
       }
    }
    return true;
  }
  let str = `isu[syv(stc]ts(crs))cs`; //false
//   let str = `isu([syvstc]ts(crs))cs`; //true
  console.log(testString(str));