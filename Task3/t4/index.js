function User(name) {
    this.drawSquare = function() {
        var canvas = document.getElementById('canvas');
        if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        ctx.fillRect(100,25,100,100);
        }
      };
      this.drawTriangle = function() {
        var canvas = document.getElementById('canvas');
        if (canvas.getContext){
            var ctx = canvas.getContext('2d');
            ctx.beginPath();
            ctx.moveTo(75,50);
            ctx.lineTo(100,75);
            ctx.lineTo(100,25);
            ctx.fill();
        }
      };
  }
  let Draw = new User();

Draw.drawSquare(); //Square
// Draw.drawTriangle(); //Square
