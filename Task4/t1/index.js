function isPowOfTwo(n) {
    return (n > 0) && (n & n - 1) == 0;
}
  console.log(isPowOfTwo(16));