let lineString =  "Lennon, John\nMcCartney, Paul\nHarrison, George\nStar, Ringo";
let arr = lineString.split(/[\n]/);
// console.log(arr);
let result = arr.map(function(name) {
    return name.replace(/(\w+), (\w+)/, "$2 $1");
});
// console.log(result);
for (let i = 0; i <result.length; i++)
console.log(result[i]);
// console.log(lineString);